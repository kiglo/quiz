#!/usr/bin/python3
import sys
import matplotlib.pyplot as plt
import matplotlib

def displayLatex(text):
    f = plt.figure(figsize=(12, 6))
    plt.plot(figure=f)
    plt.axis('off')
    matplotlib.rcParams.update({'font.size': 12})
    plt.text(-0.1, 0.5, text)
    plt.show()
