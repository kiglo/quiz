# Python 3 quiz generator

Asks questions from the file ```questions.txt``` and stores the known ones.

### Format of questions.txt

Questions are listed in section, there must be at least one section. Each section begins with a line in the following format: ```==[section name]```. Then each line contains one question, its state and optionally the answer, separated by two semicolons (;;). The state should initially set to 0. (E.g. ```question 1;;0;;answer```)

The answer can contains line breaks using the NEWLINE keyword. If an answer text starts with LATEX, then the rest of the line is going to be interpreted as a latex stucture.

### Usage

You can start the program by entering ```python3 quiz.py``` in the command line.
After running the script, one question appears. If the file of the questions contains the answer of the question, then by pressing any button, the answear will be displayed. Then entering any input different from keywords will mark the answer as completed, and the question will no longer be asked. Simply pressing enter without providing input will skip to the next question, not marking the current one. The two keywords act differently:
* exit: saves the current state then halts the program
* reset: resets the states of all questions to 0 (not marked)

