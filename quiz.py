#!/usr/bin/python3

first = 1
last = -1
import sys
import os
import platform
import latex

if platform.system().lower() == 'windows':
	clearCmd = 'cls'
else:
	clearCmd = 'clear'

args = sys.argv
if len(args) >= 3:
    first = int(args[1])
    last = int(args[2])

f = open("questions.txt", "r")
content = f.readlines()
f.close()

questions = []

curr_topic = ""
n = 0
skipped = 0
for l in content:
    if l[0:2] == "==":
        curr_topic = l[2:-1]
    else:
        q = {"topic": curr_topic}
        row = l.split(";;")
        q['q'] = row[0]
        q['skip'] = row[1].strip() == "1"
        if len(row) == 3:
            q['ans'] = row[2].strip().replace("NEWLINE", "\n")
        questions += [q]
        if q['skip']:
            skipped += 1
        n += 1

def save():
    f = open("questions.txt", "w")
    topic = ""
    for q in questions:
        if q['topic'] != topic:
            topic = q['topic']
            f.write("==" + topic + "\n")
        if q['skip']:
            skip = "1"
        else:
            skip = "0"
        ans = ""
        if 'ans' in q.keys():
            ans = ";;" + q['ans'].replace("\n", "NEWLINE")
        f.write(q['q'] + ";;" + skip + ans + "\n")    
    f.close()


def reset():
    for q in questions:
        q['skip'] = False
    save()

if skipped == n:
	reset()
	skipped = 0

from subprocess import call
from random import randint

while (1):
    call([clearCmd])
    if last == -1:
        index = randint(0, n-1)
    else:
        index = randint(first-1, last)
    while (questions[index]['skip']):
        index = randint(0, n-1)
    print("{:<40s}".format(questions[index]['topic']) + "{:>30}".format(str(skipped) + "/" + str(n)) + "\n\n")
    print(questions[index]['q'] + "\n\n")
    ans = input("Válasz: ")
    if 'ans' in questions[index].keys():
        if questions[index]['ans'] == "evince":
            os.system("evince-previewer pdfs/" + questions[index]['q'].split('.')[0] + ".pdf")
        elif questions[index]['ans'][:5] == "LATEX":
            latex.displayLatex(questions[index]['ans'][5:])
        else:
            print(questions[index]['ans'])
        ans = input("")
    if ans.lower() == "exit":
        break
    if ans.lower() == "reset":
        skipped = 0
        reset()
        continue
    if len(ans) > 0:
        questions[index]['skip'] = True
        skipped += 1
        save()
    if skipped == n:
        break

call([clearCmd])
print('Bye')
